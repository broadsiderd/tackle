
# Copyright (c) 2017-2018 by Lukasz Krol.

# This file is part of Tackle.

# Tackle is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Tackle is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Tackle.  If not, see <http://www.gnu.org/licenses/>.

draw.circular.plot = function(
  session,
  input,
  output,
  graph.globals,
  graph.dynamics
){
  output$plot_circle <- renderPlot({
    
    edges = graph.globals$int[graph.globals$int$id %in% graph.dynamics$int.brushed,]
    
    
    
    
    
    # int = ddply(edges,.(from.group, to.group),function(df){
    #   return(sum(df$interaction_effect))
    # })
    # 
    int = edges[,sum(interaction_effect),by=.(from.group, to.group)]


    

    chordDiagram(int)
  })
  
  
}