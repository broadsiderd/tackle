
# Copyright (c) 2017-2018 by Lukasz Krol.

# This file is part of Tackle.

# Tackle is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Tackle is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Tackle.  If not, see <http://www.gnu.org/licenses/>.

source("utils.R")

shinyServer(function(input, output,session) {
  
  #
  graph.globals = list(
    feat = NULL,
    int = NULL,
    groups = NULL,
    feat.perm = NULL,
    int.perm = NULL,
    feat.allowed = NULL,
    int.allowed = NULL,
    feature.values = NULL
  )
  
  graph.dynamics = reactiveValues(
    feat.brushed = NULL,
    int.brushed = NULL,
    feat.whitelist = NULL
  )
  
  
  
  gates = list(
    go = FALSE,
    init = FALSE
  )
  
  
  
  
  triggers = reactiveValues(
    #feat = FALSE,
    #int = FALSE,
    init=TRUE,
    redraw = FALSE
  )
  
  
  
  observeEvent(
    {
      input$select_data 
    },{
      cat("LOADING DATA\n")
      if (isolate(input$select_data)!="EMPTY"){
        
        
        
        
        
        if (file.exists(paste0(dir.data,"/",input$select_data,"/annotations.csv"))){
          annotations = read.table(paste0(dir.data,"/",input$select_data,"/annotations.csv"),stringsAsFactors = FALSE)
          names(annotations) = c("probe","label","group")
        } else {
          annotations = NULL
        }
        
        
        graphParams$feature.values = getFeatures(paste0(dir.data,"/",input$select_data,"/features.csv"))
        
        
        
        feat = getTotalEffects(paste0(dir.data,"/",input$select_data),annotations)
        
        int = getInteractionEffects(paste0(dir.data,"/",input$select_data),feat$feat,annotations)                 
        
        graphParams$feat.perm = feat$feat.perm
        feat = feat$feat
        graphParams$int.perm = int$int.perm
        int = int$int
        
        
        if (input$radio_displayMode=="raw"){
          feat$size = feat$size_r
          int$width = int$width_r                  
        } else {
          feat$size = feat$size_z
          int$width = int$width_z
        }
        
        
        
        
        if (file.exists(paste0(dir.data,"/",input$select_data,"/groups.csv"))){
          groups = read.table(paste0(dir.data,"/",input$select_data,"/groups.csv"))
          names(groups) = c("group","color","shape")
          graphParams$groups = groups
        } else {
          graphParams$groups = NULL
        }
        
        
        
        graphParams$feat = feat
        graphParams$int = int    
        
        brush.reset(session,input,output,graphParams)            
        
        
        graphParams$go = TRUE
        #triggers$redraw = TRUE
      } else {
        graphParams$go = FALSE
        
      }
      
    },ignoreInit = TRUE)
  
  
  
  observeEvent({
    input$radio_filterBehaviour
  },{
    if (isolate(graphParams$go)){
      print("brush reset call")
      brush.reset(session,input,output,graphParams)
    }
  },ignoreInit = TRUE)
  
  
  
  observeEvent(input$brush_te,{
    if (isolate(graphParams$go)){
      print("te trigger")
      triggers$feat = TRUE
    }
  },ignoreNULL = FALSE, ignoreInit = TRUE)
  
  observeEvent(input$brush_ie,{
    if (isolate(graphParams$go)){
      print("ie trigger")
      triggers$int = TRUE
    }
  }, ignoreNULL = FALSE,  ignoreInit = TRUE)
  
  
  
  
  
  observe({
    triggers.feat = triggers$feat
    triggers.int = triggers$int
    
    if (!isolate(triggers$init)  && isolate(graphParams$go)){
      
      print("changing scope")
      direction = isolate(input$radio_filterBehaviour)
      
      if ((direction=="Features") && (triggers.feat) ){
        print("feature mode, feat")
        feat.brushed = brushedPoints(isolate(graphParams$feat),isolate(input$brush_te))$id 
        
        if (is.null(feat.brushed) || (length(feat.brushed)==0)){
          feat.brushed = isolate(graphParams$feat)$id 
          int.allowed = isolate(graphParams$int)$id
          triggers$redraw = TRUE
          
        } else {
          int.allowed = isolate(graphParams$int)$id[
            isolate(graphParams$int)$from %in%  feat.brushed
            &
              isolate(graphParams$int)$to %in% feat.brushed
            
            ]
        }
        
        graphParams$feat.brushed = feat.brushed
        graphParams$int.allowed = int.allowed
        graphParams$int.brushed = int.allowed
        
        if (isolate(input$radio_pickingMode=="raw")){
          output$plot_ie = renderPlot(drawInteractionEffectsPlot(isolate(graphParams$int[isolate(graphParams$int)$id %in% isolate(graphParams$int.allowed),])))          
        } else {
          output$plot_ie = renderPlot(drawInteractionEffectsPlotZ(isolate(graphParams$int[isolate(graphParams$int)$id %in% isolate(graphParams$int.allowed),])))       
        }
        
        
        session$resetBrush("brush_ie") 
        triggers$feat = FALSE
      }
      
      if ((direction=="Features") && (triggers.int)){
        print("feaure mode, int")
        int.brushed = brushedPoints(isolate(graphParams$int[isolate(graphParams$int)$id %in% isolate(graphParams$int.allowed),]),isolate(input$brush_ie))$id
        if (is.null(int.brushed) || (length(int.brushed)==0)) graphParams$int.brushed = isolate(graphParams$int.allowed) else graphParams$int.brushed = int.brushed
        triggers$int = FALSE
      }
      
      
      if ((direction=="Interactions") && (triggers.int) ){
        print("interaction mode, int")
        int.brushed = brushedPoints(isolate(graphParams$int),isolate(input$brush_ie))$id 
        
        if (is.null(int.brushed) || (length(int.brushed)==0)){
          int.brushed = isolate(graphParams$int)$id 
          feat.allowed = isolate(graphParams$feat)$id
          triggers$redraw = TRUE
          
        } else {
          
          feat.allowed = isolate(graphParams$feat)$id[
            isolate(graphParams$feat)$id %in%  isolate(graphParams$int)$from[isolate(graphParams$int)$id %in% int.brushed]
            |
              isolate(graphParams$feat)$id %in% isolate(graphParams$int)$to[isolate(graphParams$int)$id %in% int.brushed]
            
            ]
        }
        
        graphParams$int.brushed = int.brushed
        graphParams$feat.allowed = feat.allowed
        graphParams$feat.brushed = feat.allowed
        if (isolate(input$radio_pickingMode=="raw")){
          output$plot_te = renderPlot(drawTotalEffectsPlot(
            isolate(graphParams$feat[isolate(graphParams$feat)$id %in% isolate(graphParams$feat.allowed),])))
          
        } else {
          output$plot_te = renderPlot(drawTotalEffectsPlotZ(
            isolate(graphParams$feat[isolate(graphParams$feat)$id %in% isolate(graphParams$feat.allowed),])))
        }
        session$resetBrush("brush_te") 
        triggers$int = FALSE
      }
      
      if ((direction=="Interactions") && (triggers$feat)){
        print("interaction mode, feat")
        feat.brushed = brushedPoints(isolate(graphParams$feat[isolate(graphParams$feat)$id %in% isolate(graphParams$feat.allowed),]),isolate(input$brush_te))$id
        if (is.null(feat.brushed) || (length(feat.brushed)==0)) graphParams$feat.brushed = isolate(graphParams$feat.allowed) else graphParams$feat.brushed = feat.brushed
        triggers$feat = FALSE
      }      
      
      
      
    }})
  
  observeEvent(
    {
      input$radio_layoutMode
    },
    {
      if (graphParams$go){
        print("layout")
        triggers$redraw = TRUE       
      }
    },
    ignoreInit = TRUE
  )
  
  observeEvent(
    {
      input$radio_displayMode
    },
    {
      if (graphParams$go){
        print("rescaling")
        if (input$radio_displayMode=="raw"){
          graphParams$feat$size =graphParams$feat$size_r
          graphParams$int$width = graphParams$int$width_r
        } else {
          graphParams$feat$size =graphParams$feat$size_z
          graphParams$int$width = graphParams$int$width_z
        }        
      }
    },
    ignoreInit = TRUE
    
  )
  
  
  observeEvent(
    {
      input$radio_pickingMode
    }
    ,
    {
      if (graphParams$go){
        print("changing picking mode")
        graphParams$feat.brushed = graphParams$feat$id
        graphParams$feat.allowed = graphParams$feat$id
        graphParams$int.brushed = graphParams$int$id
        graphParams$int.allowed = graphParams$int$id
        
        
        if (input$radio_pickingMode=="raw"){
          output$plot_te = renderPlot(drawTotalEffectsPlot(isolate(graphParams$feat)))
          output$plot_ie = renderPlot(drawInteractionEffectsPlot(isolate(graphParams$int))) 
        } else {
          output$plot_te = renderPlot(drawTotalEffectsPlotZ(isolate(graphParams$feat)))
          output$plot_ie = renderPlot(drawInteractionEffectsPlotZ(isolate(graphParams$int))) 
        }
        triggers$redraw = TRUE
        
      }
      
    },
    ignoreInit = TRUE
  )
  
  
  ###DRAWING GRAPH
  
  observe({
    dummy = triggers$redraw
    dummy = input$action_redraw    
    if (!isolate(triggers$init)  && graphParams$go && (isolate(input$action_redraw) || isolate(triggers$redraw))){
      
      
      
      print("drawing graph")
      triggers$redraw = FALSE
      
      #session$resetBrush("brush_te")
      #session$resetBrush("brush_ie")        
      
      #print(isolate(graphParams$feat)$id)
      ##print(isolate(graphParams$int)$from)
      #print(isolate(graphParams$int)$to)
      #print(isolate(graphParams$feat.brushed))
      #print(isolate(graphParams$int.brushed))
      
      
      
      #print(head(graphParams$feat))
      sliced = cutGraph(
        isolate(graphParams$feat),
        isolate(graphParams$int),
        isolate(graphParams$feat.brushed),
        isolate(graphParams$int.brushed)
      )     
      
      
      nodes = sliced$nodes2add
      edges = sliced$edges2add
      #print(nodes$id)
      #print(edges$from)
      #print(edges$to)        
      
      #print(edges$width)
      
      
      if (length(unique(nodes$group))==1)
        nodes$group = NULL
      
      nodes$font.size = node.font.size
      
      graph = visNetwork(nodes,edges) %>% 
        visEdges(color=list(color='rgba(200,200,200,10)',highlight='rgba(255,50,50,255)')) %>%
        visLegend(width = 0.1, position = "right") %>%
        visExport(float="left") %>%
        visNodes(shape="dot") %>%
        
        visEvents(
          
          
          
          selectEdge = "function(properties) {
          Shiny.onInputChange('current_edge_id', properties.edges)
          ;}"
          
          
        )
      #visEvents(
      #  doubleClick="
      #  function(properties){
      #  window.open('https://www.ncbi.nlm.nih.gov/pubmed/?term=' + this.body.data.nodes.get(properties.nodes[0]).label);
      #  }
      
      #  "
      #)
      
      
      if(!is.null(graphParams$groups)){
        groups = graphParams$groups
        print("groups")
        for (i in 1:nrow(groups)){
          graph = graph %>% visGroups(
            groupname = groups$group[i],
            color = groups$color[i],
            shape = groups$shape[i]
          )
        }
      }
      
      
      mode = isolate(input$radio_layoutMode)
      if (mode=="iGraph")
        graph = graph %>% visIgraphLayout()
      else if (mode=="Circular")
        graph = graph %>% visIgraphLayout("layout_in_circle")     
      
      
      graph %>% visPhysics(
        enabled= FALSE,
        stabilization = FALSE
      ) %>% visNodes(physics=FALSE) %>% visEdges(physics=FALSE)
      updateCheckboxInput(session,"checkbox_physics",value=FALSE)
      
      
      
      output$graph = renderVisNetwork(graph)
  }
  })
  
  
  ## MODIFYING GRAPH
  
  observeEvent(
    {
      graphParams$feat
      graphParams$int
      graphParams$feat.brushed
      graphParams$int.brushed
    },
    {
      if (graphParams$go){
        print("modifying graph")
        sliced = cutGraph(
          isolate(graphParams$feat),
          isolate(graphParams$int),
          isolate(graphParams$feat.brushed),
          isolate(graphParams$int.brushed)
        )
        
        #print("nodes to add:")
        #print(sliced$nodes2add)
        #print("nodes to del:")
        #print(sliced$nodes2del)
        #print("edges to add:")
        #print(sliced$edges2add)
        #print("edges to del:")
        #print(sliced$edges2del)
        
        
        visRemoveEdges(visNetworkProxy("graph"),sliced$edges2del)
        visUpdateEdges(visNetworkProxy("graph"),sliced$edges2add)
        
        visRemoveNodes(visNetworkProxy("graph"),sliced$nodes2del)
        visUpdateNodes(visNetworkProxy("graph"),sliced$nodes2add)          
        
      }
      
    },
    ignoreInit = TRUE
    
  )
  
  
  
  
  observeEvent(
    input$checkbox_physics,
    {
      print("physics")
      switchPhysics(input)        
      
    },ignoreInit = TRUE
  )
  
  
  observe({
    output$plot_te_large = renderPlot(drawTotalEffectsPlot(graphParams$feat[graphParams$feat$id %in% graphParams$feat.brushed,],large=TRUE))
  })
  
  observe({
    output$plot_ie_large = renderPlot(drawInteractionEffectsPlot(graphParams$int[graphParams$int$id %in% graphParams$int.brushed,],large=TRUE))
  })
  
  observe({
    output$plot_te_large_z = renderPlot(drawTotalEffectsPlotZ(graphParams$feat[graphParams$feat$id %in% graphParams$feat.brushed,],large=TRUE))
  })
  
  observe({
    output$plot_ie_large_z = renderPlot(drawInteractionEffectsPlotZ(graphParams$int[graphParams$int$id %in% graphParams$int.brushed,],large=TRUE))
  })
  
  observe({
    output$plot_te_dist = renderPlot(drawTotalEffectDistribution(
      graphParams$feat[graphParams$feat$id %in% graphParams$feat.brushed,],
      graphParams$feat.perm[graphParams$feat.perm$id %in% (graphParams$feat.brushed),]
    ))
  })
  
  observe({
    output$plot_te_dist_z = renderPlot(drawTotalEffectDistributionZ(
      graphParams$feat[graphParams$feat$id %in% graphParams$feat.brushed,]
    ))
  })
  
  observe({
    output$plot_ie_dist = renderPlot(drawInteractionEffectDistribution(
      graphParams$int[graphParams$int$id %in% (graphParams$int.brushed),],
      graphParams$int.perm[graphParams$int.perm$id %in% (graphParams$int.brushed),]
    ))
  })
  
  observe({
    output$plot_ie_dist_z = renderPlot(drawInteractionEffectDistributionZ(
      graphParams$int[graphParams$int$id %in% (graphParams$int.brushed),]
    ))
  })
  
  observe({
    output$plot_circle = renderPlot(
      drawCirclePlot(
        graphParams$int[graphParams$int$id %in% (graphParams$int.brushed),],
        graphParams$groups
      )
    )
  })
  
  
  observe({
    output$dt_feat = renderDataTable({
      DT::datatable(
        graphParams$feat[
          graphParams$feat$id %in% graphParams$feat.brushed,
          c('label','total_effect','total_effect_z_score','group')    
          ],
        colnames = c ('Feature','Raw Total Effect','Total Effect Z-Score','Group'),
        filter = 'top',
        extensions = c('Buttons','ColReorder'),
        options = list(
          pageLength = 20,
          lengthMenu = c(10, 20, 50, 100),
          dom = 'frtBip',
          buttons = c('pageLength','copy','excel','print'),
          colReorder = TRUE
        ),
        rownames = FALSE
        
      )
    })
    
  })
  
  observe({
    output$dt_int = renderDataTable({
      DT::datatable(
        graphParams$int[
          graphParams$int$id %in% (graphParams$int.brushed),
          c('from.label','to.label','interaction_effect','interaction_effect_z_score')    
          ],
        colnames = c ('Feature 1','Feature 2','Raw Interaction Effect','Interaction Effect Z-Score'),
        filter = 'top',
        extensions = c('Buttons','ColReorder'),
        options = list(
          pageLength = 20,
          lengthMenu = c(10, 20, 50, 100),
          dom = 'frtBip',
          buttons = c('pageLength','copy','excel','print'),
          colReorder = TRUE
        ),
        rownames = FALSE
        
      )
    })
    
  })
  
  
  
  observe({
    output$plot_circle <- renderPlot({
      
      edges = graphParams$int[graphParams$int$id %in% graphParams$int.brushed,]
      
      
      
      
      
      int = ddply(edges,.(from.group, to.group),function(df){
        return(sum(df$interaction_effect))
      })
      
      
      colors = graphParams$groups
      
      if(!is.null(colors)){
        grid.col = colors$color
        names(grid.col) = colors$group
      } else {
        grid.col = rainbow(length(unique(c(int$from.group,int$to.group))))
        print(grid.col)
      }
      
      
      chordDiagramFromDataFrame(int,directional=2,grid.col=grid.col)
    }
    )    
    
  })
  
  
  
  observe({
    
    
    edge = input$current_edge_id
    #print(edge)
    pl = NULL
    if (is.null(isolate(graphParams$feature.values))){
      pl = ggplot(data.frame(x=(1:1),y=(1:1),text="FEATURES UNAVAILABLE")) +
        geom_text(aes(x=x,y=y,label=text),size=3)
    } else if (length(edge)!=1){
      pl = ggplot(data.frame(x=(1:1),y=(1:1),text="SELECT ONE INTERACTION")) +
        geom_text(aes(x=x,y=y,label=text),size=3)
    } else{
      featnames = isolate(graphParams$int)[
        isolate(graphParams$int)$id==edge
        ,
        ]
      
      feat.y.name = names(isolate(graphParams$feature.values))[1]
      feat.x1.name = featnames$from.probe
      feat.x2.name = featnames$to.probe
      
      
      
      feat.y = isolate(graphParams$feature.values)[[sub("-",".",feat.y.name)]]
      feat.x1 = isolate(graphParams$feature.values)[[sub("-",".",feat.x1.name)]]
      feat.x2 = isolate(graphParams$feature.values)[[sub("-",".",feat.x2.name)]]
      
      #print(isolate(graphParams$feature.values))
      print(feat.x1.name)
      print(feat.x2.name)
      print(feat.y.name)
      print(names(graphParams$feature.values))
      
      print(feat.y)
      print(feat.x1)
      print(feat.x2)
      
      if((class(feat.x1)=="numeric") && (class(feat.x2)=="numeric") && (class(feat.y)=="character") ){
        
        pl = drawIntContContDisc(feat.x1,feat.x2,feat.y,feat.x1.name,feat.x2.name,feat.y.name)
      } else if ((class(feat.x1)=="character") && (class(feat.x2)=="character") && (class(feat.y)=="character") ){
        pl = drawIntDiscDiscDisc(feat.x1,feat.x2,feat.y,feat.x1.name,feat.x2.name,feat.y.name)
      } else if ((class(feat.x1)=="character") && (class(feat.x2)=="numeric") && (class(feat.y)=="character")){
        pl = drawIntDiscContDisc(feat.x1,feat.x2,feat.y,feat.x1.name,feat.x2.name,feat.y.name)
      } else if ((class(feat.x1)=="numeric") && (class(feat.x2)=="character") && (class(feat.y)=="character") ){
        pl = drawIntDiscContDisc(feat.x2,feat.x1,feat.y,feat.x2.name,feat.x1.name,feat.y.name)
      }
      
      
      
    }
    
    output$plot_interaction = renderPlot(pl)     
    output$plot_interaction_large = renderPlot(pl)      
    
    
    
  })
  
  
  observe({
    print("init complete")
    triggers$init = FALSE
  })
  
  
})
