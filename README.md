# Tackle
## Downloads
It is possible to download **Tackle** [here](https://drive.google.com/drive/folders/1IRBXpLMCFholYB_ncC4EhR2ygYRgnbqj?usp=sharing).

## About
Tackle is a Shiny Server based visualization dashboard for analyzing [Broadside](https://bitbucket.org/broadsiderd/broadside) results. 
Tackle allows to manipulate interactive feature interaction networks, perform literature search 
and to perform ad-hoc GO and KEGG overrepresentation tests.

## Some context
Tackle was created as unofficial part of my [PhD thesis](https://bitbucket.org/broadsiderd/broadside/src/master/broadside_directories/doc/Broadside_PhD_thesis.pdf) in order to provide
interactive visualizations for [Broadside](https://bitbucket.org/broadsiderd/broadside) as well as to enrich the results with
hypergeometric tests and literature search. 
It should be thought off as a set of R scripts rather than coherent software. 
Nevertheless it does its job, so is an option to consider when analyzing [Broadside](https://bitbucket.org/broadsiderd/broadside) results.

## How to run it
1. Clone or download the repository, or download a zipped version: **Tackle** [here](https://drive.google.com/drive/folders/1IRBXpLMCFholYB_ncC4EhR2ygYRgnbqj?usp=sharing)
2. Install R (latest version tested is 4.0.5).
3. Install references using _install_libs.R.
4. Modify tackle.bat batch script in order for it to point to the correct R version (or create a new bash script when using Linux).
5. Make sure that the whole folder is writable for the account running Tackle.
6. Launch tackle.bat, a Shiny Dashboard should open in your browser:
![Tackle](images/Tackle.png)  

## How to add new data
As an example, you can use one of the SEQC datasets in the [Downloads](https://bitbucket.org/broadsiderd/tackle/downloads/) section.
Each set of [Broadside](https://bitbucket.org/broadsiderd/broadside) results should be placed in a separate subdirectory in **data** folder.
The following files are mandatory:  
**totalEffect.csv** - feature ranking  
**interactionEffect.csv** - interaction ranking  
In addition,  further files increase functionality of the dashboard:  
**features.csv** - it will possible to visualize individual features and interactions  
**annotations.csv** - technical feature names can be replaced with labels and grouped into categories  
**keywords.csv** - mapping of feature names to keywords used for literature search  
**literature.rds** - Base for literature search, can be generated with **literature_crawler.R** script  
**hypergeo.RData** - R Objects needed for running hypergeometric tests  

## Future of Tackle
Please keep in mind that this was implemented mostly at night:) 
It probably won't be significantly altered in the current version. 
Serious refactoring is pending, possible even rewrite in a different language.