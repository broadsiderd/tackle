source("config.R")
options(stringsAsFactors = FALSE)

feat = read.table(paste0(dir.data,"/",subdir.data,"/totalEffect.csv"),header=TRUE)$feat

feat.from = unique(read.table(paste0(dir.data,"/",subdir.data,"/interactionEffect.csv"),header=TRUE)$feature1)
feat.to = unique(read.table(paste0(dir.data,"/",subdir.data,"/interactionEffect.csv"),header=TRUE)$feature2)



feat.all = unique(c(feat,feat.from,feat.to))


annotations = read.table(paste0(dir.data,"/",subdir.data,"/annotations.csv"))

annotations = annotations[annotations$V1 %in% feat.all,]

write.table(
  annotations,
  file=paste0(dir.data,"/",subdir.data,"/annotations.csv"),
  quote = TRUE, 
  sep="\t",
  row.names = FALSE, 
  col.names = FALSE            
)
