
# Copyright (c) 2017-2018 by Lukasz Krol.

# This file is part of Tackle.

# Tackle is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Tackle is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Tackle.  If not, see <http://www.gnu.org/licenses/>.

switch.doubleclick = function(
  session,
  input,
  output,
  graph.globals,
  graph.dynamics
  
  ){
  cat("DOUBLECLICK BEHAVIOR\n")
  if (input$radio_dbclick=="explore"){
    print("ok")
    visNetworkProxy("graph") %>% visEvents(type="off",doubleClick="") 
    
    visNetworkProxy("graph") %>%
    visEvents(
      doubleClick=paste0("
                         function(properties){
                            if (properties.nodes!=''){
                             window.open('",graph.globals$url,"' + this.body.data.nodes.get(properties.nodes[0]).keyword);
                            }
                          }
                         ")
      ) 
    
} else {
  
  visNetworkProxy("graph") %>% visEvents(type="off",doubleClick="")
  
  visNetworkProxy("graph") %>% 
    visEvents(
    doubleClick="
    function(properties){
            Shiny.onInputChange('current_node_id_trigger', Math.random())

    ;}
    
    "
    
  ) 
  }
}