
# Copyright (c) 2017-2018 by Lukasz Krol.

# This file is part of Tackle.

# Tackle is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Tackle is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Tackle.  If not, see <http://www.gnu.org/licenses/>.

calculate.cluster.boundaries = function(
  session,
  input,
  output,
  graph.globals,
  graph.dynamics
  
){
  cat("CALCULATING CLUSTER BOUNDARIES\n")
  
  clust = Mclust(graph.globals$feat$total_effect)$classification
  clust.levels = unique(clust)
  
  boundaries = NULL
  if (length(clust)>1)
  for (clus in clust.levels[1:(length(clust.levels)-1)]){
    boundry.lower = max(which(clust==clus))
    boundry.upper = min(which(clust==(clus-1)))
    boundaries = c(boundaries,(boundry.lower+boundry.upper)/2)
  }

  graph.globals$feat.clust.boundaries = boundaries
  
  clust = Mclust(graph.globals$int$interaction_effect)$classification
  clust.levels = unique(clust)
  
  boundaries = NULL
  if (length(clust)>1)
  for (clus in clust.levels[1:(length(clust.levels)-1)]){
    boundry.lower = max(which(clust==clus))
    boundry.upper = min(which(clust==(clus-1)))
    boundaries = c(boundaries,(boundry.lower+boundry.upper)/2)
  }
 
  graph.globals$int.clust.boundaries = boundaries
  
}