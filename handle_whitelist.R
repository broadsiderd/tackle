
# Copyright (c) 2017-2018 by Lukasz Krol.

# This file is part of Tackle.

# Tackle is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Tackle is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Tackle.  If not, see <http://www.gnu.org/licenses/>.

handle.whitelist = function(
  session,
  input,
  output,
  graph.globals,
  graph.dynamics,
  load = FALSE
){
  
  
  if (load){
    graph.globals$feat$borderWidth = 1
    graph.globals$feat$color.border = 'blue'
    graph.globals$feat$color.highlight.border = 'blue'
    
    
    graph.globals$int$color = 'grey'
    #graph.globals$int$color.highlight.background = 'blue'
    
    
    
    if (file.exists(paste0(dir.data,"/",input$select_data,"/feat_whitelist.rds"))){
      graph.dynamics$feat.whitelist = readRDS(paste0(dir.data,"/",input$select_data,"/feat_whitelist.rds"))
      graph.globals$feat$borderWidth[graph.globals$feat$id %in% graph.dynamics$feat.whitelist] = 2
      graph.globals$feat$color.border[graph.globals$feat$id %in% graph.dynamics$feat.whitelist] = 'red'
      graph.globals$feat$color.highlight.border[graph.globals$feat$id %in% graph.dynamics$feat.whitelist] = 'red'

    } else {
      graph.dynamics$feat.whitelist = numeric(0)
    }

        
    if (file.exists(paste0(dir.data,"/",input$select_data,"/int_whitelist.rds"))){
      graph.dynamics$int.whitelist = readRDS(paste0(dir.data,"/",input$select_data,"/int_whitelist.rds"))
 
      graph.globals$int$color[graph.globals$int$id %in% graph.dynamics$int.whitelist] = 'red'
    } else {
      graph.dynamics$int.whitelist = numeric(0)
    }
    
  } else {
    if (length(input$current_node_id)==1 && (input$current_node_id != -1)) {
      feat = input$current_node_id
      if (feat %in% graph.dynamics$feat.whitelist){
        graph.dynamics$feat.whitelist = setdiff(graph.dynamics$feat.whitelist,feat)
      } else {
        graph.dynamics$feat.whitelist = c(graph.dynamics$feat.whitelist,feat)
        
      }
      graph.globals$feat$borderWidth = 1
      graph.globals$feat$color.border = 'blue'
      graph.globals$feat$color.highlight.border = 'blue'
      
      graph.globals$feat$borderWidth[graph.globals$feat$id %in% graph.dynamics$feat.whitelist] = 2
      graph.globals$feat$color.border[graph.globals$feat$id %in% graph.dynamics$feat.whitelist] = 'red'
      graph.globals$feat$color.highlight.border[graph.globals$feat$id %in% graph.dynamics$feat.whitelist] = 'red'
      
      saveRDS(graph.dynamics$feat.whitelist,file=paste0(dir.data,"/",input$select_data,"/feat_whitelist.rds")) 
    }
    
    if ((length(input$current_edge_id)==1) && (input$current_edge_id!=1) ){
      int = input$current_edge_id
      if (int %in% graph.dynamics$int.whitelist){
        graph.dynamics$int.whitelist = setdiff(graph.dynamics$int.whitelist,int)
      } else {
        graph.dynamics$int.whitelist = c(graph.dynamics$int.whitelist,int)
      }
      graph.globals$int$color = 'grey'
      #graph.globals$int$color.highlight.background = 'blue' 
      graph.globals$int$color[graph.globals$int$id %in% graph.dynamics$int.whitelist] = 'red'
      saveRDS(graph.dynamics$int.whitelist,file=paste0(dir.data,"/",input$select_data,"/int_whitelist.rds"))
    }
  }
  

  
}